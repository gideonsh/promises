import { expect } from "chai";
import * as P from "../src/promise-async/promise.each";
import { Pany } from "../src/promise-async/types";



describe("promise.each", () => {
    context("each", () => {
        it("check", () => {
            expect(P.each).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = async (i: Pany) : Pany => {return await i;};
            expect(await P.each(arr, cb)).to.deep.equal([1,2,3]);
        });
    });

});


