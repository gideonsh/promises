import log from "@ajar/marker";

export const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

export const echo = async (msg: string, ms: number) => {
    log.yellow(`--> start ${msg}`);
    await delay(ms);
    log.blue(`finish <-- ${msg}`);
    return msg;
};

export const random = (max: number, min = 0): number =>
    min + Math.round(Math.random() * (max - min));
