import { Pany } from "./types.js";

export async function all(promises: Pany[]): Promise<any[]> {
    // try{
        
    const results = [];
    for (const p of promises) {
        results.push(await p);
    }
    return results;
    // }catch(error){
    //    console.log("error inside:",error.message);
    //    throw error;
    // }
}
