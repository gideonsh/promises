import { expect } from "chai";
import * as P from "../src/promise-async/promise.all";
import { Pany } from "../src/promise-async/types";


describe("async", () => {
    context("all", () => {
        it("check if a function", () => {
            expect(P.all).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            expect(await P.all(arr)).to.deep.equal([1,2,3]);
        });
    });
    
});













