import { expect } from "chai";
import * as P from "../src/promise-async/promise.utils";


describe("promise.utils", () => {
    context("delay", () => {
        it("check", () => {
            expect(P.delay).to.a("function");
        });
    });

    context("echo", () => {
        it("check", () => {
            expect(P.echo).to.a("function");
        });
        it("check in & out parameters", async () => {
            const msg = "hello";
            expect(await P.echo(msg, 3)).to.deep.equal(msg);
        });
    });

    context("random", () => {
        it("check", () => {
            expect(P.random).to.a("function");
        });
        it("check in & out parameters", async () => {
            expect(await P.random(1)).to.deep.equal(Number);
            expect(await P.random(1,5)).to.deep.equal(Number);
        });
    });

});