import { Pany, arrPany } from "./types.js";
export declare function reduce(iterable: arrPany, cb: (a: Pany, i: Pany) => Pany, initial: Pany): Promise<Pany>;
