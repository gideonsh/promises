import { expect } from "chai";
import * as P from "../src/promise-async/promise.filter";
import { Pany } from "../src/promise-async/types";



describe("promise.filter", () => {
    context("filter", () => {
        it("check", () => {
            expect(P.filterSeries).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = (i: Pany) : Pany => {return i;};
            expect(await P.filterSeries(arr, cb)).to.deep.equal([1,2,3]);
        });
    });
    
    
    
    
    context("filter", () => {
        it("check", () => {
            expect(P.filterParallel).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = (i: Pany) : Pany => {return i;};
            expect(await P.filterParallel(arr, cb)).to.deep.equal([1,2,3]);
        });
    });
    

});



