import { iObjP } from "./types.js";

export async function props(promisesObj: iObjP): Promise<iObjP> {
    const results: iObjP = {};
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
