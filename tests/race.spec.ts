import { expect } from "chai";
import * as P from "../src/promise-async/promise.race";
import { Pany } from "../src/promise-async/types";


describe("promise.race", () => {
    context("race", () => {
        it("check", () => {
            expect(P.race).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            expect(await P.race(arr)).to.deep.equal(2);
        });
    });

    context("some", () => {
        it("check", () => {
            expect(P.some).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            expect(await P.some(arr, 3)).to.deep.equal([1,2,3]);
        });
    });
    

});
















