export type Pany = Promise<any>;
export type PanyArr = Promise<any[]>;
export type arrPany = Promise<any>[];
export type iObjP = {
    [k: string]: Pany;
};
