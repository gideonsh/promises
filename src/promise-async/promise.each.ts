import { Pany, arrPany } from "./types.js";

export async function each(
    iterable: arrPany,
    cb: (i: Pany) => Pany
): Promise<arrPany> {
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}
