import { Pany, arrPany } from "./types.js";
export declare function each(iterable: arrPany, cb: (i: Pany) => Pany): Promise<arrPany>;
