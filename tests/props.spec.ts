import { expect } from "chai";
import * as P from "../src/promise-async/promise.props";
import { iObjP } from "../src/promise-async/types";


describe("promise.props", () => {
    context("props", () => {
        it("check", () => {
            expect(P.props).to.a("function");
        });
        it("check in & out parameters", async () => {
            const ob : iObjP = {};
            ob["first"] = new Promise((resolve) => {resolve(1);}); 
            ob["second"] = new Promise((resolve) => {resolve(2);});
            ob["third"] = new Promise((resolve) => {resolve(3);});
            const result = {
                "first": 1,
                "second": 2,
                "third": 3,
            };

            expect(await P.props(ob)).to.deep.equal(result);
        });
    });
    

});










