import { Pany, arrPany } from "./types.js";
export declare function filterSeries(iterable: arrPany, cb: (i: Pany) => Pany): Promise<string | Promise<any>[]>;
export declare function filterParallel(iterable: arrPany, cb: (i: Pany) => Pany): Promise<string | Promise<any>[]>;
