import { Pany, arrPany } from "./types.js";
export declare function mapParallel(iterable: arrPany, cb: (i: Pany) => Pany): Promise<any[]>;
export declare function mapSeries(iterable: arrPany, cb: (i: Pany) => Pany): Promise<string | any[]>;
