import { expect } from "chai";
import * as P from "../src/promise-async/promise.map";
import { Pany, arrPany } from "../src/promise-async/types";



describe("promise.map", () => {
    context("map", () => {
        it("check", () => {
            expect(P.mapSeries).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = (i: Pany) : Pany => {return i;};
            expect(await P.mapSeries(arr, cb)).to.deep.equal([1,2,3]);
        });
    });


    context("map", () => {
        it("check", () => {
            expect(P.mapParallel).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = (i: Pany) : Pany => {return i;};
            expect(await P.mapParallel(arr, cb)).to.deep.equal([1,2,3]);
        });
    });
    

});







