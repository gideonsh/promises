export declare type Pany = Promise<any>;
export declare type PanyArr = Promise<any[]>;
export declare type arrPany = Promise<any>[];
export declare type iObjP = {
    [k: string]: Pany;
};
