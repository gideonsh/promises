import { Pany, arrPany } from "./types.js";

export async function reduce(
    iterable: arrPany,
    cb: (a: Pany, i: Pany) => Pany,
    initial: Pany
): Promise<Pany> {
    let aggregator = initial || iterable[0];

    for (const item of iterable) {
        aggregator = await cb(aggregator, item);
    }

    return aggregator;
}
