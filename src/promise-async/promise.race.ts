import { arrPany } from "./types.js";

export async function race(promises: arrPany): Promise<any> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}

export async function some(promises: arrPany, num: number): Promise<arrPany> {
    const results: arrPany = [];
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
