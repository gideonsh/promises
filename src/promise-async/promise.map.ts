import { Pany, arrPany } from "./types.js";

export async function mapParallel(iterable: arrPany, cb: (i: Pany) => Pany) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

export async function mapSeries(iterable: arrPany, cb: (i: Pany) => Pany) {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}
