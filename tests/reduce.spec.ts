import { expect } from "chai";
import * as P from "../src/promise-async/promise.reduce";
import { Pany } from "../src/promise-async/types";



describe("promise.reduce", () => {
    context("reduce", () => {
        it("check", () => {
            expect(P.reduce).to.a("function");
        });
        it("check in & out parameters", async () => {
            const arr : Pany[] = [
                new Promise((resolve) => {resolve(1);}), 
                new Promise((resolve) => {resolve(2);}),
                new Promise((resolve) => {resolve(3);}),
            ];
            const cb = (i: Pany) : Pany => {return i;};
            const initial = new Promise((resolve) => {resolve(0);});
            expect(await P.reduce(arr, cb, initial)).to.deep.equal(6);
        });
    });

});
